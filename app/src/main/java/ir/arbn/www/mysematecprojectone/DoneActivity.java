package ir.arbn.www.mysematecprojectone;

import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class DoneActivity extends AppCompatActivity {
    TextView regLable2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);
        regLable2 = (TextView) findViewById(R.id.regLable2);
        regLable2.setText(getIntent().getStringExtra("username"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Registration is Done", Toast.LENGTH_LONG).show();

    }
}
