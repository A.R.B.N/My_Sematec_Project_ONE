package ir.arbn.www.mysematecprojectone;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {
    EditText regTxt;
    Button regBtn;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        regTxt = findViewById(R.id.regTxt);
        regBtn = findViewById(R.id.regBtn);

        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(HomeActivity.this, DoneActivity.class);
                intent.putExtra("username", regTxt.getText().toString());
                startActivity(intent);

            }
        });
        Toast.makeText(this, "Welcome Strangers, To My Project ", Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Are You New? Register Now", Toast.LENGTH_SHORT).show();

    }
}
